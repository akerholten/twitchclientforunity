﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.ComponentModel;
using System.Net.Sockets;
using System.IO;

public class TwitchClient : MonoBehaviour {

    [SerializeField] private string username;
    [SerializeField] private string channelName;
    [SerializeField] private string tokenEnvironmentVariable = "TwitchBotAuth";
    [SerializeField] private string ircChannel = "irc.chat.twitch.tv";
    [SerializeField] private int ircPort = 6667;

    private string oauthToken;
    //private float pingCooldown, pingTimer;

    private TcpClient client;
    private NetworkStream stream;
    private StreamReader reader;
    private StreamWriter writer;

    // I feel like the scene is loaded several times, because the bug where client becomes null is whenever I go back to Unity, like if it loads something over
    // Whenever I edit code, save it, and go back to the running version, the client becomes null
    void Start () {
        oauthToken = Environment.GetEnvironmentVariable(tokenEnvironmentVariable);
        ConnectToChannel();
        //pingCooldown = pingTimer = 10.0f;
    }
	
	void Update () {
        if (client == null || !client.Connected) // This check sometimes throws a null reference exception, something is not working as it should. maybe do !client?.Connected
        {
            Debug.Log("Disconnected, reconnecting..");
            ConnectToChannel();
        }
        /*pingTimer -= Time.deltaTime;
        if (pingTimer <= 0)
        {
            KeepConnectionAlive();
            pingTimer = pingCooldown;
        }*/


        ReadChat();
    }

    private void ConnectToChannel()
    {
        client = new TcpClient(ircChannel, ircPort);
        stream = client.GetStream();
        reader = new StreamReader(stream);
        writer = new StreamWriter(stream);

        writer.WriteLine("PASS " + oauthToken);
        writer.WriteLine("NICK " + username);
        writer.WriteLine("USER " + username + " 8 * :" + username);
        writer.WriteLine("JOIN #" + channelName);
        writer.WriteLine("CAP REQ :twitch.tv/commands"); // To enable whispers
        writer.Flush();
    }

    private void ReadChat()
    {
        if(stream.DataAvailable)
        {
            var message = reader.ReadLine();
            Debug.Log(message);
            if (message.Contains("PRIVMSG"))
            {
                // Getting the user's username by splitting it from the string
                var splitPoint = message.IndexOf("!", 1);
                var userName = message.Substring(0, splitPoint);
                userName = userName.Substring(1);

                // Getting the user's message by splitting from the string
                splitPoint = message.IndexOf(":", 1);
                message = message.Substring(splitPoint + 1);
                Debug.Log(userName + ": " + message);
                if (message.ToLower().Contains("ping"))
                {
                    WriteMessage("pong");
                }
            }
            else if (message.Contains("WHISPER"))
            {
                // Getting the user's username by splitting it from the string
                var splitPoint = message.IndexOf("!", 1);
                var userName = message.Substring(0, splitPoint);
                userName = userName.Substring(1);

                // Getting the user's message by splitting from the string
                splitPoint = message.IndexOf(":", 1);
                message = message.Substring(splitPoint + 1);
                Debug.Log("WHISPER: " + userName + ": " + message);
                if (message.ToLower().Contains("ping"))
                {
                    WriteWhisper("pong", userName);
                }
            }
            else if (message.Contains("PING"))
            {
                KeepConnectionAlive();
            }
        }
    }

    private void WriteMessage(string m)
    {
        writer.WriteLine("PRIVMSG #" + channelName + " :" + m);
        writer.Flush();
    }

    private void WriteWhisper(string m, string user)
    {
        writer.WriteLine("PRIVMSG #" + channelName + " :/w " + user + " " + m);
        writer.Flush();
    }

    private void KeepConnectionAlive()
    {
        // This works to keep connection alive, but should maybe do it some other way so it could work with other irc clients?
        writer.WriteLine("PONG :tmi.twitch.tv");
        writer.Flush();
    }
}
